" PLUGINS (install plugins to '~/.vim/plugged' using vim-plug)
call plug#begin('~/.vim/plugged')

Plug 'https://github.com/joshdick/onedark.vim'
Plug 'https://github.com/sheerun/vim-polyglot'
Plug 'https://github.com/itchyny/lightline.vim'

call plug#end()


" COLORSCHEME (https://github.com/joshdick/onedark.vim)
syntax on
colorscheme onedark


" STATUSLINE
set noshowmode          " disable default mode display (e.g. -- INSERT --) as lightline does this now


" SPACES AND TABS
set tabstop=4           " 4 space tab
set expandtab           " use spaces for tabs
set softtabstop=4       " 4 space tab
set shiftwidth=4        " size of an 'indent', measured in spaces
filetype indent on
filetype plugin on
set autoindent          " copy the indentation from the previous line, when starting a new line


" LINE NUMBERS
set number                     " show current line number on cursor line
set relativenumber             " show relative line numbers on all lines except the cursor line
hi LineNr ctermfg=yellow       " display line numbers in white.
set cursorline                 " highlight current line.            


" FINDING FILES (from https://www.youtube.com/watch?v=XA2WjJbmmoM&t=408s)
" search down into subfolders and provide tab-completion for all file-related tasks
set path+=**

" display all matching files when tab-completing
set wildmenu

" usage:
"   - hit tab to :find by partial match
"   - use '*' to make it fuzzy
"   - :b lets you autocomplete any open buffer
