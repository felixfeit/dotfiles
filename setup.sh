# vim setup
    # vim-plug
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    
    # download .vimrc file
    curl -fLo ~/.vim/vimrc --create-dirs https://gitlab.com/felixfeit/dotfiles/-/raw/master/.vimrc

    # download .tmux.conf file
    curl -fLo ~/.tmux.conf --create-dirs https://gitlab.com/felixfeit/dotfiles/-/raw/master/.tmux.conf
    