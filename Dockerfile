FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    tmux \
    curl \
    vim \
    && rm -rf /var/lib/apt/lists/*

RUN source <(curl -s https://gitlab.com/felixfeit/dotfiles/-/raw/master/setup.sh)
